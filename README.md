# Information

This study aims to compare errors amongst various algorithms in predicting binary classification of parole relapse among inmates.

The data from this project comes from the following Kaggle repository:

https://www.kaggle.com/econdata/predicting-parole-violators

The data makes use of several factors including:
1. demographic factors (i.e. age,race)
2. Criminal background (i.e. offense type, multiple offenses)

The algorithm attempts to classify if these inmates commited parole violation upon release.
